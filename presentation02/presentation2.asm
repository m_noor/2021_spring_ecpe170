.text  # include <stdio.h>
.globl main


    # $s0 = array
    # $s1 = arraySize
    # $s2 = i
    # $s3 = result
    # $t0/$t1 = bytes
    # $t2 = array element at eevry 4 bytes

#int arraySum(int* array, int arraySize);

main: #int main(){
   #int array[] = {2, 3, 5, 7, 11};
   #int arraySize = 5;
   #int i;
    #This create 5 temporary variables that will later be used to 
    lw $s0, array
    lw $s1, arraySize
    lw $s2, i
    li $t0, 0 #temp variable that will iterate by 4 to access each element in array
   #for(i=0; i<arraySize; i++){
      #printf("Array[%i]=%i\n", i, array[i]);}
   #printf("Sum of array is %i\n", arraySum(array, arraySize ));}
#print statements within for loop


for_loop:
    #if i == arraySize, exit loop
    beq  $s2, $s1, arraySum  # if t9 == 10 we are done
    
    li $v0, 4 #"Array["
    la $a0, print_1
    syscall
    
    li $v0, 1 #Array at index i
    move $a0, $s2
    syscall

    li $v0, 4 #"]="
    la $a0, print_2
    syscall
    
    li $v0, 1 #prints the element at index i (done through bytes)
    lw $a0, array($t0)
    syscall
    
    li $v0, 4
    la $a0, newLine #"\n"
    syscall
    

    addi $s2, $s2, 1 #i increments by 1
    addi $t0, $t0, 4 #incrememnets by four to run through array
    j for_loop #runs through the for loop till the condition to break it is met
    
    
#int arraySum(int* array, int arraySize){
   #int result;

   #return result;}
arraySum:
    addi $sp,$sp,-4 # Adjust stack pointer
    sw $ra,0($sp) # Save $ra
    
    beq $s1, 0, if #if arraySize == 0
    j else #else arraySize != 0
    
    lw $s3, result # initalizes result = 0 by default unless it goes through else statement
    li $t1, 0 #temp var to keep track of array elements
   #if(arraySize == 0)
      #result = 0;
if:
    li $v0, 4  #"The array is empty. \n"
    la $a0, printingResult0 
    syscall
    j printSum
  
   #else
      #result = *array + arraySum(&array[1], arraySize-1);
   
else: 
    beq $t1, 20, printSum #goes to diff function once 20 bytes are reached/aka the array has beeen run through
    lw $t2, array($t1) #temp var that holds each value of each element in the array
    add $s3, $s3, $t2 #adds it to the registar holding the sum val
    addi $t1, 4 #increments through every 4 bytes / means each array element
    j else #make it loop through the else till the entire array is read
    
    lw $ra,0($sp) # Restore $ra
    addi $sp,$sp,4 # Adjust stack pointer
    
printSum: #prints sum 
    li $v0, 4 #"Sum of array is "
    la $a0, printingSumString
    syscall
    
    li $v0, 1 #prints the value of the sum
    move $a0, $s3
    syscall
    j exit #exits functions

exit:
    li $v0, 10 # Sets $v0 to "10" to select exit syscall
    syscall # Exit
    
.data
#.space because it is an array
array: .word 2 3 5 7 11 #array holding values to be used 
arraySize: .word 5
i: .word 0
result: .word 0
print_1: .asciiz "Array["
print_2: .asciiz "]="
newLine: .asciiz "\n"
printingResult0: .asciiz "The array is empty. \n"
printingSumString: .asciiz "Sum of array is "
