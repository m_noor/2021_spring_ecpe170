#!/usr/bin/env python3

# Python DNS query client
#
# Example usage:
#   ./dns.py --type=A --name=www.pacific.edu --server=8.8.8.8
#   ./dns.py --type=AAAA --name=www.google.com --server=8.8.8.8

# Should provide equivalent results to:
#   dig www.pacific.edu A @8.8.8.8 +noedns
#   dig www.google.com AAAA @8.8.8.8 +noedns
#   (note that the +noedns option is used to disable the pseduo-OPT
#    header that dig adds. Our Python DNS client does not need
#    to produce that optional, more modern header)

from urllib.parse import urlparse
import argparse
import ctypes
import random
import socket
import struct
import sys

def main():


	name = "Mah Noor"

	server = "cyberlab.pacific.edu"
	server_ip = urlparse(server).netloc #server_ip = "54.148.163.48"

	port = 3456

	server_address = (server_ip, port)

	int_A_num = 1 
	int_B_num = 2

##############################################################

	    # Create UDP socket
	    # ---------
	print("Created UDP socket")
	s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
	    # ---------
###############################################################

	    # Generate DNS request message
	    # ---------
	print("Generated DNS request message")
	endianness = "!B"
	endianness2 = "!L"
	version_num = 1
		    
	version = struct.pack(endianness, version_num) 
	A = struct.pack(endianness2,int_A_num)
	B = struct.pack(endianness2,int_B_num)
	nm = struct.pack(endianness2,len(name))
	x = name.encode()
	    # ---------
	   
	   #used source https://www.programcreek.com/python/example/91711/dns.flags
	    
	combined = version + A + B + nm + x
	print ("Version " + str(version_num) + " is defined as "+ str(version) + " in hexadecimal. \n")
	print ("My first random int was " + str(int_A_num)+ " which is defined as "+ str(A) + " in hexadecimal. \n")
	print ("My second random int was " + str(int_B_num)+ " which is defined as "+ str(B) + " in hexadecimal. \n")
	print("My name is "+ name +". The length of my name (plus a space to separate it) is defined as " +str(len(name)) + ". In hexadecimal it makes "+str(nm)+ ".\n")
	print("\nCombined together is: "+str(combined))
################################################################################################################################################	    
	    # Send request message to server
	    # (Tip: Use sendto() function for UDP)
	    # ---------
	print("Sent request message to server")
	s.sendto(combined,(server, port))
#######################################################
	    #Recieved Message
	print("Recieved request message by server")
	max_bytes = 4096
	(totBytes, src_addr) = s.recvfrom(max_bytes)
	(x, y, z) = struct.unpack(endianness+"LH", totBytes)
	v = int_A_num+int_B_num
	if z is v:
	     print("\n\nSum is " + str(v))
	     print("Success!")
	else:
	     print("\n\nFail *cue sad music*\n")
########################################################
	    # Close socket
	    # ---------	
	print("Closed Socket")
	s.close()
########################################################
if __name__ == "__main__":
    sys.exit(main())
