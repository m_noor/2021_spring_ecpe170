.text  # include <stdio.h>
.globl main

main: # int main(){

   # declaring all the integer values
   li $s0, 15# int A=15;
   li $s1, 10# int B=10;
   li $s2, 7 # int C=7;
   li $s3, 2 # int D=2;
   li $s4, 18# int E=18;
   li $s5, -3# int F=-3;
   li $s6, 0 # int Z=0;

   # since assembly code requires only one step at a time, 
   # I put each piece of the expression into temporary variables
   # then later after line 24, I add or subtract those temporary
   # variables together
   # Z = (A+B) + (C-D) + (E+F) - (A-C);
   add $t1, $s0, $s1 # tempVar1=A+B
   sub $t2, $s2, $s3 # tempVar2=C-D
   add $t3, $s4, $s5 # tempVar3=E+F
   sub $t4, $s0, $s2 # tempVar4=A-C
   
   
   # Z = tempVar1 + tempVar2 + tempVar3 - tempVar4
   add $s6, $t1, $t2, # Z = tempVar1 + tempVar2 --> Z = (A+B) + (C-D)
   add $s6, $s6, $t3 # Z += tempVar3 aka--> Z += (E+F)
   sub $s6, $s6, $t4 # Z -= tempVar4 aka--> Z -= (A-C) 

   sw $s6, Z # puts the final value inside Z (avoids out of bound array)
   li $v0, 10 # Sets $v0 to "10" to select exit syscall
   syscall # exit
   
.data #All memory structures are placed after.data 
Z: .word 0 # stores memory
