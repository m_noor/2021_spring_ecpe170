.text  # include <stdio.h>
.globl main

main: # int main(){

	   # declaring all the integer values
	lw $s0, A # int A=10;
	lw $s1, B # int B=15;
	lw $s2, C # int C=6;
	lw $s3, Z # int Z=0;
	   
	   # if(A > B || C < 5)
	   #  Z = 1;
	 
	   #if(A > B || C < 5) --> takes care of the conditions of the first for loop
	bgt $s0, $s1, first_case #if A is greater than B or 
	blt $s2, 5, first_case #if C is less than 3  
	j else_if	     #then it goes to the firstcase where Z is initialized to 2

	   #takes care of the first if-statement in the code
	first_case:  
		li $s3, 1 #sets Z to the 1, first if statement is met (Z=1)
		j switch
	   #-------------------------------------------------------------------------
	  
	   # if ((C+1) == 7 && ) since it is an && in the conditions, both need to be met which 
	   # means that if both consitions are met, it stays in 2nd case, otherwise it goes 
	   # to the next if statement which is the third case
	else_if:
		add $t1,$s2,1 # tempVar1 = C+1--> tempVar1 = $s2+1//need to store it in a separate 
				  # var to ensure the value of C is unaffected 
			beq $t1, 7, second_case #  if tempVar == 7, go to next case
          		j else

			    #else if((A > B) && ((C+1) == 7))
			    #Z = 2;
			second_case:  
				ble $s0, $s1, else # checks for the opposite of the second part of the if 
						      # statement so that the programs remains here if the 
						      # condition is met and changes if the condition is unmet
				li $s3, 2 # Z = 2 (equivalent to how I did it in the first if-statement)
				j switch
	  #--------------------------------------------------------------------------  
	    #else
	    #  Z = 3;
	    #else statement that takes care of any condition that is not met in the 2 if statements
	else:
		third_case:
			li $s3, 3 # Z = 3
			j switch
	  #--------------------------------------------------------------------------------
	  #The next portion will be using the Z to officially set Z to a final value
	   # switch(Z)
	    # {  (all the j switch statements at the end of each if is how we get here)
	    
	switch:
	     # case 1:
	      #   Z = -1;
	       #  break;
	       
		beq $s3, 1, first_case_checked # if Z = 1 is then it goes to the first_case_checked function 
	     # case 2:
	      #   Z -=-2;
	       #  break;
		beq $s3, 2, second_case_checked # if Z = 2 than go to second_case_checked
	      #default:
	       #  Z = 0;
	       # break;
	 
		j default_case_checked # if alls the other conditions don't work, then it goes to this
	     			    # case in the switch statement
		
		first_case_checked:
			li $s3, -1 # Z = -1   
			j end #break
	   
		second_case_checked:
			sub $s3, $s3, -2 # Z -= -2
			j end #break

		default_case_checked:
			li $s3, 0 # Z = 0
			j end
	      
	end:
		sw $s3, Z #Stores Z with the value that is being held in $s3
		li $v0, 10 # Sets $v0 to "10" to select exit syscall

syscall # Exit

.data #All memory structures are placed after.data 
A: .word 10
B: .word 15
C: .word 6
Z: .word 0 # stores memory
