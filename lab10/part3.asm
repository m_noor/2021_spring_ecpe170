.text  # include <stdio.h>
.globl main

main: #int main() 
    # declaring all the integer values
    lw $s0, Z # int Z = 2
    lw $s1, i # int i = 0
    li $t1, 1 #temporary variable to run the incrementing by 1
    li $t2, 2 #temporary variable to run the incrementing by 2
    # This function is holding the first while loop of the fucntion
    # while(1){
    # if(i>20)
    #   break;
    # Z++;
    # i+=2;
   #}
#   while(1){
while1:
    bgt $s1, 20, do_while # a conditional statement that takes program to another 
    			   # function once this function is completed (since i 
    			   # increments by 2 later on, it will eventually reach 20
    add $s0, $s0, $t1 # Z += 1; increments Z by 1
    add $s1, $s1, $t2 # i+=2; increments i by 2
    j while1 #Loops to itself unti the condition of i >20 is fulfilled
#   }

#   do {
#      Z++;
#   } while (Z<100);
# This is a do while loop that will increment Z by 1 till Z is larger than or equal to 100
do_while:
    add $s0, $s0, $t1 # Z += 1; increments Z by 1
    bge $s0, 100, decrement_Z_and_i # exits while loop onces Z is larger
     				     # than or equal to 100
    j do_while #continue doing the do-while loop if (Z<100)

#   while(i > 0) {
#      Z--;
#      i--;
#   }  
  
# this is the second while loop that decrements the vales of i and
# z till i reaches 0 (and Z = 78)
decrement_Z_and_i:
    ble $s1, 0, store #condition that ends the while-loop when (i>0) 
    sub $s0, $s0, $t1 # Decrements Z by one(Z = Z-1)
    sub $s1, $s1, $t1 # Decrements i by one (i = i-1)
    j decrement_Z_and_i #continue doing the while loop if (i > 0)
   
   # once all the loops are completed, the ending value is stored in 
   # this function for both Z and i 
store:
   sw $s0, Z #Stores Z with the value of $s0
   sw $s1, i #Stores i with the value of $s1


    li $v0, 10 # Sets $v0 to "10" to select exit syscall
    syscall # Exit

    # All memory structures are placed after the .data
.data
    #where values can be changed
Z:  .word 2 #Z=2
i:  .word 0 #i=0




