# include <stdio.h>

int main()
{
   int A[5]; // Empty memory region for 5 elements
   int B[5] = {1,2,3,4,5};
   int C=12;
   int i;
   
   for(i=0; i<5; i++)
   {
      printf("%d A[i] before \n\n", A[i]);
      A[i] = B[i] + C;
      printf("%d %d\n B Val w i", i, B[i]);
      printf("%d A[i] after solving \n\n\n", A[i]);
   } 

   i--;
   printf("%d i \n\n\n", i);
   
   while(i >= 0)
   {
      printf("%d A after while loop \n\n", A[i]);
      A[i]=A[i]*2;
      i--;
      printf("%d\n A after x2 ", A[i]);
      printf("%d i after decrement \n\n\n", i);
   } 
}
