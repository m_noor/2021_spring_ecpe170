.text  # include <stdio.h>
.globl main

main: #int main() 
 #for(i=0; i<5; i++){
      #A[i] = B[i] + C;} 
   #i--;
   #while(i >= 0){
      #A[i]=A[i]*2;
      #i--;} 




   # This will represent the variables where A and B are loaded in and set
    
   #int A[5]; // Empty memory region for 5 elements
   #int B[5] = {1,2,3,4,5};
   #int C=12;
   #int i;
  #  la $s0, A #Loads in the empty A array; int A[5]= {}
  #  la $s1, B #loading the starting address
    lw $s2, C #int C = 12
    lw $s3, i #int i
    li $t9, 4 #this here is a temp var holding 4 for later incrementation
    
    #This create 5 temporary variables that will later be used to 
    # initilize array B
    lw $t1, 1 #tempVar1 =1
    lw $t2, 2 #tempVar2 =2
    lw $t3, 3 #tempVar3 =3
    lw $t4, 4 #tempVar4 =4
    lw $t5, 5 #tempVar5 =5
    
    #this is where the B at the correct index are actually initialized
    sw $t1, 0($s1)  #B at 0th index = tempVar1 =1
    sw $t2, 4($s1)  #B at 1st index = tempVar2 =2
    sw $t3, 8($s1)  #B at 2nd index = tempVar3 =3
    sw $t4, 12($s1) #B at 3rd index = tempVar4 =4
    sw $t5, 16($s1) #B at 4th index = tempVar5 =5
        
      #for(i=0; i<5; i++){
     # A[i] = B[i] + C;} 
    # This is a for loop that adds 12 to each value in B and stores it in 
    #the empty A array.
#   for(i=0; i<5; i++){
for_loop:
    li $t8, 16
    bgt $s3, $t8, i_incrementing_down #stays in for loop as long as the index does not go over the bytes 
    				      #20 allocated. Since the incrementation is by 4, it checks up to 16 
    lw $t6, B($s3) # this creates a temporary variable that holds all the index values of the B array
    add $t7, $t6, $s2 # this makes another temporary variable that holds the values we want to put into the empty array
#      A[i] = B[i] + C;}
    sw $t7, A($s3) # this combines the temporary variable thats holding the value we want in A and putting it in
    add $s3, $s3, $t9 # this increments the index by 4 so that it can get to the next value (as there are 4 bytes per array value)
    j for_loop #calls itself till the condition to break the loop is met
    
    #i--, this function increments i down once by the value of 4
i_incrementing_down:
    sub $s3, $s3, $t9 # i=i - 4 #increments down by 4 once
    			# of the loop to get to the next part  


   #while(i >= 0){
   #   A[i]=A[i]*2;
     # i--;  } 
while:
    sub $t8, $t8, $t8 #reuse this variable by making it equal to 0
#   while(i >= 0){
    blt $s3, $t8, exit # checks for opposite of i >=0 to break the loop
    lw $t7, A($s3) # Takes the value of A
      #A[i]=A[i]*2;
    mul $t7, $t7, 2 # mutiplies the Array by itself
    sw $t7, A($s3) # stores the new value of it being multiplied by 2 into the array
     # i--; } 
    sub $s3, $s3, 4 # decrements the index(i) by 4 (traverses backward in the array)
    j while #repeats loop till condition to break it out

exit:
    li $v0, 10 # Sets $v0 to "10" to select exit syscall
    syscall # Exit

    # All memory structures are placed after the
    # .data assembler directive
.data
#.space because it is an array
A: .space 20 #empty array holding the byte size to be used --> A{}
B: .space 20 #Holding specific values in the B array B{1,2,3,4,5}
C: .word 12
i: .word 0
