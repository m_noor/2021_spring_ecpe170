.text  # include <stdio.h>
.globl main

main: #int main() 
 # char string[256];
	li $a1, 256            # immediately loads the maximum number of bytes into a variable
 	li $v1, 8              # immediately loads the register value with the system call code for read_string
 	la $a0, string         # loads the address of the string
 	syscall # triggers exception that transfers control to kernal space
 	
 # int i=0;
 	
 # char *result = NULL;  // NULL pointer is binary zero
	la $s1, result #takes the result char into a variable
	
 # byte e loaded in
	lb $s2, letter_e # loading bytes of letter_e into $s2 which is 'e'
 # later used as index for array
 	lw $s0, i #takes the int i = 0 I initialized at the bottom
	
 # ascii values of the print statements loaded
 	li $v1, 4                        # immediately loads the register value with the system call code for print_string
 	#lw $a1, string_request_statement #requests string from user
 	#lw $s5, newline_print_statement  # loads in the new line "\n"  string
 	syscall
 	

 # // Obtain string from user, e.g. "Constantinople"
 # scanf("%255s", string); 
 
  
 # // Search string for letter 'e'.
 # // Result is pointer to first e (if it exists)
 # // or NULL pointer if it does not exist
 
 # while(string[i] != '\0') {
 while_loop:
 	lb $t0, string($s0) #creates a temp var that is keep track through an array
 	#lw $t1, $zero   #creates a temporary variable that hold "\0" in it
 	beq $s0, $zero, exit  #says that if the array being run through is empty, then to exit the while loop 
 	
 	#if(string[i] == 'e') {
 	if_string_is_e:
		 #     result = &string[i]; 
		 #     break; // exit from while loop early  }
		beq $t0, $s2, if_result_not_NULL #compares the value of array at the index with e 
		 #   i++; }
		addi $s0, $s0, 1 #increasing the index by 1 until it gets through the entire while loop
		j while_loop #until the entire loop is done, it keeps calling itself
 

 # if(result != NULL) {
 if_result_not_NULL:
	sw $t0, result #storing the 
        li $v1, 4 # immediately loads the register value with the system call code for print_string
	 #   printf("First match at address %d\n", result);
 	lw $a2, first_print_statement   # loads in the "First match at address " string
 #	lw $s5, newline_print_statement # loads in the new line "\n"  string
 	syscall
        li $v1,1               
	la $a0, result         
	syscall
	li $v1,4               
 #   printf("The matching character is %c\n", *result); }
 	lw $a0, second_print_statement  # loads in "The matching character is " string
 	#la $a0, newline_print_statement # loads in the new line "\n"  string
	syscall
 # else
 #   printf("No match found\n");}
 	lw $s4, third_print_statement   # loads in the "No match found" string
 #	lw $s5, newline_print_statement # loads in the new line "\n"  string

exit:
    li $v0, 10 # Sets $v0 to "10" to select exit syscall
    syscall # Exit

    # All memory structures are placed in the .data
.data
    #where values can be changed
string: .space 256 #initializes string to have space of 256 bytes
i: .word 0 #initializes i to the integer 0 
result: .word 0 #initializes result to 0 which is NULL
letter_e: .byte 'e' #since letter is a char, it only needs 1 byte or data
#string_request_statement: .asciiz "Please enter a string "
first_print_statement: .asciiz "First match at address " #prints the statement before addres into ascii values
second_print_statement: .asciiz "The matching character is " #prints the statement before addres into ascii values
third_print_statement: .asciiz "No match found" #prints the statement before addres into ascii values
newline_print_statement: .asciiz "\n"
#null_char: .word "\0"
