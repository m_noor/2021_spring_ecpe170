# include <stdio.h>

int main()
{
   int Z=2;
   int i;

   i=0;
   
   printf("%i\n", Z);
   printf("%i\n\n", i);
   
   printf("First while loop is being done until i reaches 20\n");
   while(1){
     if(i>20)
       break;
    Z++;
    i+=2;
    printf("%i\n", Z);
    printf("%i\n\n", i);

   }
 //  printf("%i\n", Z);
 //  printf("%i\n\n", i);
   
   printf("First do while loop is being done until Z reaches 99\n");
   do {
      Z++;
        printf("%i\n", Z);
	printf("%i\n\n", i);

   } while (Z<100);
//   printf("%i\n", Z);
//   printf("%i\n\n", i);
   printf("Second while loop is being done until i gets to 0\n");
   while(i > 0) {
      Z--;
      i--;
        printf("%i\n", Z);
	printf("%i\n\n", i);
   }
   
 //  printf("%i\n", Z);
 //  printf("%i\n\n", i);
return 0;
}
