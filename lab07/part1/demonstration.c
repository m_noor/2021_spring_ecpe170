#include <stdlib.h>
#include <stdio.h>
#include<stdint.h>

int main(){
int ROWS = 2, COLS =3;
uint32_t a[ROWS][COLS];
uint32_t array1[1][3];// An 2-dimensional array of 4-byte (32-bit) unsigned integers, i.e. uint32_t
uint32_t array2[1][3][5];// An 3-dimensional array of 4-byte (32-bit) unsigned integers, i.e. uint32_t

//printf("2d array:\n");
for(int i = 0; i<1; i++){
 for(int j = 0; j<3; j++){
  printf("Address for element in the 2d array array1[%i][%i]:",i, j);
  printf("%p\n", &array1[i][j]);
 }
}
printf("\n\n");

//printf("3d array: \n");
for(int i = 0; i<1; i++){
 for(int j = 0; j<3; j++){
  for(int z = 0; z<5; z++){
    printf("Address for element in the 3d array array2[%i][%i][%i]:" , i, j,z);
    printf("%p\n", &array2[i][j][z]);
  }
 }
printf("\n\n");
}
/*
void sumarrayrows(int a[ROWS][COLS]) 
{ 
    int i, j, sum = 0;

    for (i = 0; i < ROWS; i++) 
        for (j = 0; j < COLS; j++)
            sum += a[i][j];
     	    printf("Sumarrayrows: %d", sum);

}

 

void sumarraycols(int a[ROWS][COLS]) 
{ 
    int i, j, sum = 0;

    for (j = 0; j < COLS; j++)
        for (i = 0; i < ROWS; i++) 
            sum += a[i][j];
     	    printf("Sumarraycols: %d", sum);
}

sumarrayrows(a[ROWS][COLS]);

sumarraycols(a[ROWS][COLS]); */

return 1;
}
