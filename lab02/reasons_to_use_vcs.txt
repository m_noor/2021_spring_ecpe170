1. Version history allows to revert back in case of a mistake.
2. Can be used to check whether a specific change is resulting in the error for a code.
3. It can be used to work in teams and see changes made by teammates.

4. Can be used to check the amount of progress made by one specific person.
5. Can allow team members to work independently without interfering with the working code of the master branch.
