// Multi-dimensional array code based on FAQ at:
// http://c-faq.com/aryptr/dynmuldimary.html
#include <stdio.h>   // Allows printf, ...
#include <string.h>
#include <stdlib.h>  // Allows malloc, ...
#include <errno.h>   // Allows errno
#include <ctype.h>

struct board{
  int row, column;
  char** userDisplayedBoard;
  char**  AnswerKeyBoard;

  };
 
 
/***************************************************************************************/


char** createArray(int rows, int cols) {
  char** myArray;
  
  // Allocate a 1xROWS array to hold pointers to more arrays
  myArray = calloc(rows, sizeof(char *));
  if (myArray == NULL) {
    printf("\n FATAL ERROR: out of memory: %s\n", strerror(errno));
    exit(EXIT_FAILURE);
  }
  
  // Allocate each row in that column
  for (int i = 0; i < rows; i++) {
    myArray[i] = calloc(cols, sizeof(char));
    if (myArray[i] == NULL) {
      printf("\n FATAL ERROR: out of memory: %s\n", strerror(errno));
      exit(EXIT_FAILURE);
    }
  }
  
  return myArray;
}


/***************************************************************************************/


void fillArray(int** myArray, int rows, int cols) {
  char unknownValues = '#';
  
  for (int i = 0; i < rows; i++) {
    for (int j = 0; j < cols; j++) {
      myArray[i][j] = unknownValues;
    }
  }
  
  return;
}


/***************************************************************************************/


void printArray(char** myArray, int rows, int columns) {

printf("    1  2  3     4  5  6     7  8  9\n");
  for (int i = 0; i < rows; i++) {
  	if (i % 3 == 0){ //adds the "boundaries" for each box of the sudoku puzzle
  	//horizonally and vertically (since its 3X3)	
    printf("   |--------------------------------|\n");
    }
    printf("%i  |", i+1);
		for (int j = 0; j < columns; j++) {
		        if ((j % 3 ==0) && (j != 0)){
		            printf("|  ");
		        }
            if (myArray[i][j]==0){
              printf("# ");
            }
		        printf("%c  ",myArray[i][j]);
		}
		printf("|\n");
    	}
    	printf("   |--------------------------------|\n");
}


/***************************************************************************************/


void deleteArray(char** myArray, int rows) {
  for (int i = 0; i < rows; i++) { //only need rows because deleting one row at a time does the same thing as removing one column at a time
    free(myArray[i]);
  }
  free(myArray);
  
  return;
}


/***************************************************************************************/


int userIntCheck(int digit){
      int checkPassed = 1; 
      /*hard codes to make sure checkPassed if equal to 1 means that the below if
      	statement was not met. This will come in handy when printing out the numbers in
      	which we will check and only change numbers if checkPassed is equal to 2.*/
      if (digit > 0 && digit < 10){
      	checkPassed = 2;
      }
      return checkPassed;
} 

int userCharCheck(char nondigit){ //similar to the userIntCheck except this one is for characters instead of integers.
      int checkPassed = 1;
      if (nondigit >= 49 && nondigit <= 57){
      	checkPassed = 2;
      }
      return checkPassed;
} 


/***************************************************************************************/


void accessFile(FILE* open, char** userDisplayGameBoard, char** answerKey,int row, int col){
  char check[2]; //an array of two which checks up to three characters at a time and moves on... this includes the space, the potential star, and then the digit, if the first one is not a star, then it can be assumed that the rest do not need to be checked as only the ones with a star can be displayed
  for(int i = 0; i < row; i++){
 	for(int j =0; j < col; j++){
	 	fscanf(open,"%s",check);
	 	if(check[0] != '*'){
		answerKey[i][j] = check[0];
		userDisplayGameBoard[i][j] = '#'; //tells the program that if there is not a star, it is a place the user has to guess and so the hashtag represents guesses needed.
	 	} 
		 else{
		 	answerKey[i][j] = check[1]; //says if the second check happens to display the value since it is avcalue that had a star attached to it
			userDisplayGameBoard[i][j] = check[1];
		 }
	 } 
   }
}


/***************************************************************************************/


int completedBoardCheck(char** board, int row, int column){
	int checked = 1, temp, win = 0;
 
 	 for (int i = 0; i < 7; i+=3) { //checks the row, column, and then each square. It checks whether all the values are filled within the puzzle as well as whether certain values need to be hashtags or real numbers.
	    for (int j = 0; j < 7; j+=3) {
	    temp = 0;
	      for (int k = 0; k < 3; k++) {
	     	for (int l = 0; l < 3; l++) {
     			temp = temp+board[i+k][j+l] - '0';
	    	}
	      }
	      if (temp == 45){
	      	win++;
	      }
	    
	    }
	  }
	  if (win != 9){ //all 9 squares have to win inorder for it to mean that the game is completed.
		checked = 0;	
	  }
	  //add all rows
	  temp = 0; win = 0;
	  for (int q = 0; q < row; q++) {
	    for (int r = 0; r < column; r++) {
	      temp= temp+ board[q][r] - '0';
	    }
	    if (temp == 45){ //1+2+3...+9 = 45---> this means if the entire row is filled, it should equal 45.
	      	win++;
	    }
	     temp = 0;
	  }
	  if (win != 9){
	 	checked = 0;	
	  }
	  //add all columns
	  temp = 0;win = 0;
	  for (int x = 0; x < row; x++) {
	    for (int y = 0; y < column; y++) {
	    	temp= temp+ board[y][x] - '0';
	    }
	    if (temp == 45){
	    	win++; //adds up each time a square, column, OR row are complete and then compares the value of the win to 9 for the number of rows, colums or squares.
	     }
	     temp = 0;
	  }
	  if (win != 9){
	  	checked = 0;	
	  }
	  return checked;
}


/***************************************************************************************/


int valuesFilledByBoard(char** board, int r, int c){
	int b = 1; //makes it known that the hashtag in the board is representing the value of zero as to when the board checks for 45 as the completed values.
	for (int i = 0; i < r; i++) {
	    for (int j = 0; j < c; j++) {
	      if (board[i][j] == '#'){
	      	b = 0;
	      }
	    }
	  }
	  return b;
}
/***************************************************************************************/

