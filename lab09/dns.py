#!/usr/bin/env python3

# Python DNS query client
#
# Example usage:
#   ./dns.py --type=A --name=www.pacific.edu --server=8.8.8.8
#   ./dns.py --type=AAAA --name=www.google.com --server=8.8.8.8

# Should provide equivalent results to:
#   dig www.pacific.edu A @8.8.8.8 +noedns
#   dig www.google.com AAAA @8.8.8.8 +noedns
#   (note that the +noedns option is used to disable the pseduo-OPT
#    header that dig adds. Our Python DNS client does not need
#    to produce that optional, more modern header)


from dns_tools import dns  # Custom module for boilerplate code

import argparse
import ctypes
import random
import socket
import struct
import sys

from dns_tools import dns_header_bitfields

def main():

    # Setup configuration
    parser = argparse.ArgumentParser(description='DNS client for ECPE 170')
    parser.add_argument('--type', action='store', dest='qtype',
                        required=True, help='Query Type (A or AAAA)')
    parser.add_argument('--name', action='store', dest='qname',
                        required=True, help='Query Name')
    parser.add_argument('--server', action='store', dest='server_ip',
                        required=True, help='DNS Server IP')

    args = parser.parse_args()
    qtype = args.qtype
    x = dns_header_bitfields()
    qname = args.qname
    server_ip = args.server_ip
    port = 53
    server_address = (server_ip, port)
    rand = random.randrange(1,50000,1);
    

    if qtype not in ("A", "AAAA"):
        print("Error: Query Type must be 'A' (IPv4) or 'AAAA' (IPv6)")
        sys.exit()

    # Create UDP socket
    # ---------
    print("Created UDP socket")
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    # ---------


    # Generate DNS request message
    # ---------
    print("Generated DNS request message")
    req_var = "Sending request for " + qname + ", type " + qtype + ", to server " + server_ip + ", port %d"% port
   #raw_bytes =  bytes(req_var, 'ascii')
   # print(req_var)
    # ---------
   
   #used source https://www.programcreek.com/python/example/91711/dns.flags
    siteName = qname.split(".") #separates to read into bytes by the period
    raw_bytes = bytearray()
    x.qr = 0
    x.rcode = 0
    x.tc = 0
    x.opcode = 0
    x.ra = 0
    x.aa = 0
    x.rd = 1
    x.reserved = 0
    b = bytes(x)
    
    endianness = "!B"
    for x in siteName:
     chars = x.encode()
     y = len(x)
     raw_bytes += struct.pack(endianness, y) 
     raw_bytes += chars
    
    raw_bytes += struct.pack(endianness,0)  
    endianness2 = "!H"
    quest =1
    anw = 0
    auth = 0
    add = 0

        
    a = struct.pack(endianness2, rand) 
    c = struct.pack(endianness2,1)#1 bit when A
    d = struct.pack(endianness2,28)#28 bits when AA
    
     
    question =struct.pack(endianness2, quest) 
    answer = struct.pack(endianness2, anw) 
    authority = struct.pack(endianness2, auth) 
    additional = struct.pack(endianness2, add) 
    totBytes = a+b+question+answer+authority+additional+raw_bytes
    
    if qtype == "A": #for A type
    	totBytes += c
    else: #for AA type
    	totBytes += d
    	
    totBytes += c
    
  #  print(totBytes)
    # Send request message to server
    # (Tip: Use sendto() function for UDP)
    # ---------
    print("Sent request message to server")
    bytes_sent= s.sendto(totBytes, server_address)
    # ---------


    # Receive message from server
    # (Tip: use recvfrom() function for UDP)
    # ---------
    print("Recieved message from server")
    max_bytes = 1028#from lecture
    (totBytes, src_addr) = s.recvfrom(max_bytes)
    # ---------


    # Close socket
    # ---------
    print("Closed Socket")
    s.close()
    # ---------


    # Decode DNS message and display to screen
    dns.decode_dns(totBytes)


if __name__ == "__main__":
    sys.exit(main())
