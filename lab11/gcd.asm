.text  # include <stdio.h>
.globl main
	#loading in word 

	main:  #int main(){
	
		 #      uint32_t n1, n2;  
		 #this loads the index, and the two numbers to be used in the function to registers
		 lw $s0, n1 #loads the values of n1 into $s0
		 lw $s1, n2 #loads the values of n2 into $s1
		 #      int i=0;
		 lw $s2, i #loads the value of i into $s2
		 
		 
		#this for loop controls the entire program. We will be calling all the 
		#functions and then breaking it. The functions will be defined 
		#after the for loop, however they will only be used with in the for loop/
		     #   for(i=0;i<10;i++) {  
		for:
		bge $s2, 10, exit #when i is greater than or equivalent to 10, it breaks the for loop, hence ending the program by going to the exit function
		#when there are arguements in a function, we need to use the $a registers before jal to include them in the function
		#          n1=random_in_range(1,10000);
		#	  n2=random_in_range(1,10000); 
		li $a0, 1 #loads in this first arguement for the random in range; high
		li $a1, 10000 #loads the second arguement for the random in range ; low
		
		jal random_in_range #calls the functions random_in_range
		move $s0, $v0 #sets $v0 from the random_in_range function and stores it to $s0
		sw $s0, n1 #stores the values of n1 into $s0 which means it is stores in memory
		
		jal random_in_range #calls the functions random_in_range
		move $s1, $v0 #sets $v0 from the random_in_range function and stores it to $s1
		sw $s1, n2 #stores the values of n1 into $s1 which means it is stores in memory
		
		move $a0, $s0 #sets the first arguement to $s0 (n1)
		move $a1, $s1 #sets the second arguement to $s1 (n2)
		jal gcd #runs the gcd fucntion w the two arguements
		move $s7, $v0 #saves the value found in the gcd function to a temporary variable
		sw $s7, common_denom #saves the value of $s7 to the common_denom value
		
		# printf("\n G.C.D of %u and %u is %u.", n1, n2, gcd(n1,n2));	}
		#"\n G.C.D of"
		li $v0, 4
		la $a0, print1
		syscall

		#%u for n1
		li $v0, 1
		lw $a0, n1
		syscall

		# " and "
		li $v0, 4
		la $a0, print2
		syscall

		#%u for n2
		li $v0, 1
		lw $a0, n2
		syscall

		#" is "
		li $v0, 4
		la $a0, print3
		syscall

		#%u for gcd(n1,n2) value
		li $v0, 1
		lw $a0, common_denom
		syscall

		#"."
		li $v0, 4
		la $a0, print4
		syscall
		
		addi $s2, $s2, 1 # increases the inex by 1 each time the for loop ends
		j for
		#       return 0;    }
		#program ends
###########################################################################################
	#uint32_t random_in_range(uint32_t low, uint32_t high){
	#$a0 = low
	#$a1 = high
	random_in_range:
		#pushes the registars from the stack in the normal order
		addi $sp,$sp,-4 # Adjust stack pointer
		sw $t0,0($sp) # Save $t0
		addi $sp,$sp,-4 # Adjust stack pointer
		sw $t1,0($sp) # Save $t1
		addi $sp,$sp,-4 # Adjust stack pointer
		sw $t2,0($sp) # Save $t2
		addi $sp,$sp,-4 # Adjust stack pointer
		sw $t3,0($sp) # Save $t3
		addi $sp,$sp,-4 # Adjust stack pointer
		sw $t4,0($sp) # Save $t4
		addi $sp,$sp,-4 # Adjust stack pointer
		sw $ra,0($sp) # Save $ra
   
		lw $t0, range #this temporary variable loads the values of range into $t0
		lw $t1, random #this temporary variable loads the values of random into $t1
		#  uint32_t range = high-low+1;
		sub $t0, $a1, $a0 # temporary varibale that does high- low (but since it is unsigned it is subu )
		addi $t0, $t0, 1 #this adds the temporary variable holding high - low and adds one to it (but since it is unsigned it is addu)
		 # uint32_t rand_num = get_random();
		jal get_random #function call that runs another function
		move $t1, $v0 #the $v0 holds the returned value of the get_random function and stores it in to the temporary variable $t1 which is random
		  #return (rand_num % range) + low;}
		
		divu $t1, $t0 #this divides the unsigned values of random($t0) and range($t1)
		mfhi $t3 #the remainder of the divu in the previous line is put into this temporary variable which is the high since we are looking at the modules
		add $t4, $t3, $a0 #adds low to the value of $t3 which is the modeules of random and range
		move $v0, $t4 #saves the value of the return function which is random%range + low
		
		#pops the registars from the stack in reverse order
		lw $ra,0($sp) # Restore $ra
		addi $sp,$sp,4 # Adjust stack pointer
		lw $t4,0($sp) # Restore $t4
		addi $sp,$sp,4 # Adjust stack pointer
		lw $t3,0($sp) # Restore $t3
		addi $sp,$sp,4 # Adjust stack pointer
		lw $t2,0($sp) # Restore $t2
		addi $sp,$sp,4 # Adjust stack pointer
		lw $t1,0($sp) # Restore $t1
		addi $sp,$sp,4 # Adjust stack pointer
		lw $t0,0($sp) # Restore $t0
		addi $sp,$sp,4 # Adjust stack pointer
#######################################################################################
	#uint32_t gcd(uint32_t n1, uint32_t n2)  {
 	gcd:
		#pushes the registars from the stack in the normal order
		addi $sp,$sp,-4 # Adjust stack pointer
		sw $s0,0($sp) # Save $s0
		addi $sp,$sp,-4 # Adjust stack pointer
		sw $s1,0($sp) # Save $s1
		#addi $sp,$sp,-4 # Adjust stack pointer
		#sw $t0,0($sp) # Save $t0
		addi $sp,$sp,-4 # Adjust stack pointer
		sw $ra,0($sp) # Save $ra
		
		move $s0, $a0 #brings the n1 argument into this function
		move $s1, $a1 #brings the n2 argument into this function
		
		 #       if (n2 != 0)
		 if:
		 beq $s1, 0, else #goes to the else statment if the if statmentif not met. The if statement is != but we have to check the opposite by doing ==
		  #         return gcd(n2, n1%n2);
			move $a0, $s1 #sets the first argument to $s1(n2)
			
			divu $s0, $s1 #this divides the unsigned values of n1($s0) and n2($s1)
			mfhi $a1 #the remainder of the divu in the previous line is put into this temporary variable which is the high since we are looking at the modules
			#move $a1, $t0 #saves this the second argument of the gcd function
			jal gcd #runs the gcd program once both arguments are taken in
			j end #goes to end function
		   #     else 
		else:
		    #       return n1; }
	   		move $v0, $s0 #returns the value of n1 ($s0) by saving it in to $v0 if the if-statement is not met
	   	
	   	end: #finishes the program
		   	
 		#pops the registars from the stack in reverse order
		lw $ra,0($sp) # Restore $ra
		addi $sp,$sp,4 # Adjust stack pointer
		#lw $t0,0($sp) # Restore $t0
		#addi $sp,$sp,4 # Adjust stack pointer
		lw $s1,0($sp) # Restore $s1
		addi $sp,$sp,4 # Adjust stack pointer
		lw $s0,0($sp) # Restore $s0
		addi $sp,$sp,4 # Adjust stack pointer


#######################################################################################		    
	#uint32_t get_random(){
		#// Generate random 32-bit unsigned number
		#// based on multiply-with-carry method shown
		#// at http://en.wikipedia.org/wiki/Random_number_generation
	get_random:
		#everything from this function comes directly from the professors lectures.

		#pushes the registars from the stack in the normal order
		addi $sp, $sp, -4
		sw $s0, 0($sp)
		addi $sp, $sp, -4
		sw $s1, 0($sp)  
		addi $sp, $sp, -4
		sw $t0, 0($sp)
		addi $sp, $sp, -4
		sw $t1, 0($sp)  
		addi $sp, $sp, -4
		sw $t8, 0($sp)  
		addi $sp, $sp, -4
		sw $t9, 0($sp)  
		addi $sp, $sp, -4
		sw $ra, 0($sp)

		#  m_w = 18000 * (m_w & 65535) + (m_w >> 16);
		lw $s0, m_w #read m_w from the memory
		srl $t0, $s0, 16 #m_w >> 16, this makes the $s0 shift to the right by 16 bytes
		andi $t1, $s0, 65535 #m_w & 65535
		li $t8, 18000    
		mul $t2, $t1, $t8 # 18000 * (m_w &65535)
		addu $s0, $t2, $t0 # 18000 * (m_w & 65535) + (m_w >> 16);
		sw $s0, m_w # restores word into $s0

		#  m_z = 36969 * (m_z & 65535) + (m_z >> 16);
		lw $s1, m_z #read m_z from memory
		srl $t0, $s1, 16 #m_z >> 16 this makes the $s1 shift to the right by 16 bytes
		andi $t1, $s1, 65535 #m_z & 65535
		li $t9, 36969    
		mul $t2, $t1, $t9 # 36969 * (m_z &65535)
		addu $s1, $t2, $t0 # 36969 * (m_z & 65535) + (m_w >> 16);
		sw $s1, m_z # restores word into $s0

		lw $s0, m_w
		lw $s1, m_z
		sll $v0, $s1, 16 #m_z<<16
		#the caller will get return call from v0
		addu $v0, $v0, $s0 #(m_w << 16) + m_w

		#save the result in v0
		return:  

		#callee must retreive ra
		lw $ra, 0($sp)
		addu $sp, $sp, 4

		#pops the registars from the stack in reverse order
		lw $t9, 0($sp)
		addi $sp, $sp, 4
		lw $t8, 0($sp)
		addi $sp, $sp, 4
		lw $t1, 0($sp)
		addi $sp, $sp, 4
		lw $t0, 0($sp)
		addi $sp, $sp, 4
		lw $s1, 0($sp)
		addi $sp, $sp, 4
		lw $s0, 0($sp)
		addi $sp, $sp, 4

		#  return result;}
		# Return from function
		jr $ra # Jump to addr stored in $ra

	#  uint32_t result;
	#  result = (m_z << 16) + m_w;  /* 32-bit result */

########################################################################################
	exit: 
		li $v0, 10 # Sets $v0 to "10" to select exit syscall
		syscall # exit
########################################################################################
.data #All memory structures are placed after.data 
#uint32_t m_w = 50000;
#uint32_t m_z = 60000;
m_w: .word 50000 # initializes the values of m_w to 50000
m_z: .word 60000 # initializes the values of m_z to 60000
n1: .word 1 # initializes (temporarily) the value of n1 to one
n2: .word 1 # initializes (temporarily) the value of n2 to one
i: .word 0 # intializes index to 0
range: .word 0
random: .word 0
print1: .asciiz "\n G.C.D of "
print2: .asciiz " and "
print3: .asciiz " is "
print4: .asciiz ".(periodt)"
common_denom: .word 0
