#include <stdio.h>
#include <string.h>
int attempts = 0;
int letterNum = 0;
char answerKey1[] = "h a n g m a n";
char answerKey2[] = "H A N G M A N";
char emptyToFull[] = "_ _ _ _ _ _ _";
char wrongattempts[] = "        ";
//////////////////////////////////////////////////////////////////////////////////////////////////////
void lose_statement(){
  printf("\n\nSorry buddy, you lost.\n\n");
}

void win_statement(){
  printf("\n\nHip Hip, Hooray! You win! \n\n");
}
//////////////////////////////////////////////////////////////////////////////////////////////////////
void isCorrect(){
printf("correct guess \n-----------------\n");
//printf("%s", emptyToFull);
}

void isWrong(char g){
    printf ("wrong guess \n-----------------\n");
    attempts+=1;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////
void iterate(char g){
  int temp = 0;
  for(int i = 0; answerKey1[i]!='\0'; i++){
    if (answerKey1[i] == g){
      emptyToFull[i] = g;
      letterNum+=1;
      temp =1;
    }
    
  }
  for(int i = 0; answerKey2[i]!='\0'; i++){
    if (answerKey2[i] == g){
      emptyToFull[i] = g;
      letterNum+=1;
      temp =1;
    }
    
  } 
  if (temp == 1){
    isCorrect();
  }
  else if(temp == 0){
    isWrong(g);
     for(int i = 0; wrongattempts[i]!='\0'; i++){
    if (wrongattempts[i] == ' '){
      wrongattempts[i] = g;
      break;
    }
   }
  }
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
void print_hangman(){
  if (attempts == 0){       
  printf("\n\n  |-----|  \n  |     |\n        |  \n        |\n        |\n        |\n        |\n -----------\n");
  printf("\n\nIncorrect Attempts: %d\n", attempts);
  printf("\nEverything incorrect you tried: %s\n", wrongattempts);
  printf("\n\nWord: %s\n",emptyToFull);
  }

  else if (attempts == 1){ 
  printf("\n\n  |-----|  \n  |     |\n  O     |  \n        |\n        |\n        |\n        |\n -----------\n");
  printf("\n\nIncorrect Attempts: %d\n", attempts);
  printf("\nEverything incorrect you tried: %s\n", wrongattempts);
  printf("\n\nWord: %s\n",emptyToFull);
  }
  else if (attempts == 2){
  printf("\n\n  |-----|  \n  |     |\n  O     |  \n  |     |\n  |     |\n        |\n        |\n -----------\n");
  printf("\n\nIncorrect Attempts: %d\n", attempts);
  printf("\nEverything incorrect you tried: %s\n", wrongattempts);
  printf("\n\nWord: %s\n",emptyToFull);
  }
  else if (attempts == 3){
  printf("\n\n  |-----|  \n  |     |\n  O     |  \n /|     |\n  |     |\n        |\n        |\n -----------\n");
  printf("\n\nIncorrect Attempts: %d\n", attempts);
  printf("\nEverything incorrect you tried: %s\n", wrongattempts);
  printf("\n\nWord: %s\n",emptyToFull);
  }
  else if (attempts == 4){
  printf("\n\n  |-----|  \n  |     |\n  O     |  \n /|\\    |\n  |     |\n        |\n        |\n -----------\n");
  printf("\n\nIncorrect Attempts: %d\n", attempts);
  printf("\nEverything incorrect you tried: %s\n", wrongattempts);
  printf("\n\nWord: %s\n",emptyToFull);
  }
  else if (attempts == 5){
  printf("\n\n  |-----|  \n  |     |\n  O     |  \n /|\\    |\n  |     |\n /      |\n        |\n -----------\n");
  printf("\n\nIncorrect Attempts: %d\n", attempts);
  printf("\nEverything incorrect you tried: %s\n", wrongattempts);
  printf("\n\nWord: %s\n",emptyToFull);
  }
  else if (attempts == 6){
  printf("\n\n  |-----|  \n  |     |\n  O     |  \n /|\\    |\n  |     |\n / \\    |\n        |\n -----------\n");
  printf("\n\nIncorrect Attempts: %d\n", attempts);
  printf("\nEverything incorrect you tried: %s\n", wrongattempts);
  printf("\n\nWord: %s\n",emptyToFull);
  lose_statement();
  }
}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
int main(void) {
  char g;
  printf("\n Welcome to Hangman! \n Version 1.0 \n Implemented by Mah Noor\n\n");
  print_hangman();
  printf("\n\nPlease enter a guess (A-Z to play or 0 to Exit): ");
  scanf(" %c", &g);
  if ( g=='0'){
  	printf("\n\n Exited game\n\n");
  	}
  else{
  iterate(g);
  print_hangman();

  while(attempts <= 6 && letterNum <7 && g!='0'){
  printf("\n\nPlease enter a guess (A-Z to play or 0 to Exit): ");
  scanf(" %c", &g);
  if ( g=='0'){
    printf("\n\n Exited game\n\n");
    break;
  }
  iterate(g);
  print_hangman();
  if(attempts == 6){
    break;
    }
  
  }
  if(letterNum == 7){
  win_statement();
  }
  }

}

