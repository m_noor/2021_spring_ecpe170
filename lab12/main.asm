#include <stdio.h>
#include <string.h>
.text
.globl main 

lw $s0, attempts
lw $s1, letterNum


#///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 		#int main(void) {
 	main:
		#  char g;
		#  printf("\n Welcome to Hangman! \n Version 1.0 \n Implemented by Mah Noor\n\n");
		li $v0, 4
		la $a0, startStatement
		syscall
		#  print_hangman();
		jal print_hangman
		#  printf("\n\nPlease enter a guess (A-Z to play or 0 to Exit): ");
	getUserInput:
		li $v0, 4 #reads in string
		la $a0, getUserInputStatement #gives user instruction to guess
		syscall 
		#  scanf(" %c", &g);
		li $v0, 8 # getting user input
		la $a0, guess # address to store char at
		li $a1, 2 # maximum number of chars (including '\0')
		syscall
		li $v0, 4
		la $a0, newLine
		syscall
		la $t0, answerKey1   # load the address of the array into $a1
		lb $t1, 0($t0)
		lb $t2, 1($t0)
		lb $t3, 3($t0)
		lb $t4, 4($t0)	
		lb $t5, 5($t0)	
		lb $t6, 6($t0)		
		bge $a2, guess, print_winstatement
		
		la $a1, answerKey2   # load the address of the array into $a1
		lb $a2, 0($a1)           # load a byte from the array into $a2
		addi $a2, $a2, 1         # increment $a2 by 1
		sb $a2, 0($a1)           # store the new value into memory
		addi $a1, $a1, 1         # increment $a1 by one, to point to the next element in the array
		
		bge $a2, guess, print_winstatement
		bne $a2, guess, print_losestatement
		jal print_user_input
	print_user_input:
		li $v0, 4
		la $a0, guess # address to store string at
		syscall
		jal print_losestatement
		#  if ( g=='0'){
		#  	printf("\n\n Exited game\n\n");
		#  	}
		#  else{
		#  iterate(g);
		#  print_hangman();
		#
		#  while(attempts <= 6 && letterNum <7 && g!='0'){
		   while_loop_in_main:
		#  printf("\n\nPlease enter a guess (A-Z to play or 0 to Exit): ");
		#  scanf(" %c", &g);
		#  if ( g=='0'){
		#    printf("\n\n Exited game\n\n");
		#    break;
		#  }
		#  iterate(g);
		#  print_hangman();
		#  if(attempts == 6){
		#    lose_statement();
		#    break;
		#    }
		#  
		#  }
		#  if(letterNum == 7){
		#  win_statement();
		#  }
		#  }
		#
		#}

#//////////////////////////////////////////////////////////////////////////////////////////////////////
		#char lose_statement(){
	#lose_statement:
		#  printf("\n\nSorry buddy, you lost.\n\n");}
	print_losestatement:
		li $v1, 4 #reads in string 
		la $a0, loseStatement #takes in lose statement string from data
		syscall
		jal getUserInput
	#win_statement:
		#void win_statement(){
		#  printf("\n\nHip Hip, Hooray! You win! \n\n");}
	print_winstatement:
		li $v1, 4 #reads in string 
		la $a0, winStatement #takes in win statement string from data
		syscall
		jal getUserInput
#//////////////////////////////////////////////////////////////////////////////////////////////////////
		#void isCorrect(){
		#printf("correct guess \n");
		#//printf("%s", emptyToFull);
		#}
	isCorrect:
		li $v1, 4 #reads in string 
		la $a0, correctGuessStatement #takes in lose statement string from data
		syscall
		#void isWrong(char g){
		#    printf ("wrong guess \n");
		#    attempts+=1;
		#}
	isWrong:
		li $v1, 4 #reads in string 
		la $a0, wrongGuessStatement #takes in lose statement string from data
		syscall
#/////////////////////////////////////////////////////////////////////////////////////////////////////////
		#void iterate(char g){
		#  int temp = 0;
		#  for(int i = 0; answerKey1[i]!='\0'; i++){
		#    if (answerKey1[i] == g){
		#      emptyToFull[i] = g;
		#      letterNum+=1;
		#      temp =1;
		#    }
		#    
		#  }
		#  for(int i = 0; answerKey2[i]!='\0'; i++){
		#    if (answerKey2[i] == g){
		#      emptyToFull[i] = g;
		#      letterNum+=1;
		#      temp =1;
		#    }
		    
		#  } 
		#  if (temp == 1){
		#    isCorrect();
		#  }
		#  else if(temp == 0){
		#    isWrong(g);
		#     for(int i = 0; wrongattempts[i]!='\0'; i++){
		#    if (wrongattempts[i] == ' '){
		#      wrongattempts[i] = g;
		#      break;
		#    }
		#   }
		#  }
		#}
		#
#/////////////////////////////////////////////////////////////////////////////////////////////////////
			print_hangman:
				#void print_hangman(){
				li $t0, 0
				beq $s0, $t0, if_printhangman0
				beq $s0, 1, if_else_printhangman1
				beq $s0, 2, if_else_printhangman2
				beq $s0, 3, if_else_printhangman3
				beq $s0, 4, if_else_printhangman4
				beq $s0, 5, if_else_printhangman5
				beq $s0, 6, if_else_printhangman6
				#  if (attempts == 0){       printf("\n  |-----|  \n  |     |\n        |  \n        |\n        |\n        |\n        |\n -----------\n");
				#  printf("\n\nIncorrect Attempts: %d\n", attempts);
				#  printf("\nEverything incorrect you tried: %s\n", wrongattempts);
				#  printf("\n\nWord: %s\n",emptyToFull);
				#  }
				#
				if_printhangman0:
					li $v1, 4
					la $a0, mistakeHangman_0
					syscall
					jal getUserInput			
				#  else if (attempts == 1){ 
				#  printf("\n\n  |-----|  \n  |     |\n  O     |  \n        |\n        |\n        |\n        |\n -----------\n");
				#  printf("\n\nIncorrect Attempts: %d\n", attempts);
				#  printf("\nEverything incorrect you tried: %s\n", wrongattempts);
				#  printf("\n\nWord: %s\n",emptyToFull);
				#  }
				if_else_printhangman1:
					li $v1, 4
					la $a0, mistakeHangman_1
					syscall
					#jal while_loop_in_main			
				#  else if (attempts == 2){
				#  printf("\n\n  |-----|  \n  |     |\n  O     |  \n  |     |\n  |     |\n        |\n        |\n -----------\n");
				#  printf("\n\nIncorrect Attempts: %d\n", attempts);
				#  printf("\nEverything incorrect you tried: %s\n", wrongattempts);
				#  printf("\n\nWord: %s\n",emptyToFull);
				#  }
				if_else_printhangman2:
					li $v1, 4
					la $a0, mistakeHangman_2
					syscall
					#jal while_loop_in_main			
				#  else if (attempts == 3){
				#  printf("\n\n  |-----|  \n  |     |\n  O     |  \n /|     |\n  |     |\n        |\n        |\n -----------\n");
				#  printf("\n\nIncorrect Attempts: %d\n", attempts);
				#  printf("\nEverything incorrect you tried: %s\n", wrongattempts);
				#  printf("\n\nWord: %s\n",emptyToFull);
				#  }
				if_else_printhangman3:
					li $v1, 4
					la $a0, mistakeHangman_3
					syscall
					#jal while_loop_in_main			
				#  else if (attempts == 4){
				#  printf("\n\n  |-----|  \n  |     |\n  O     |  \n /|\\    |\n  |     |\n        |\n        |\n -----------\n");
				#  printf("\n\nIncorrect Attempts: %d\n", attempts);
				#  printf("\nEverything incorrect you tried: %s\n", wrongattempts);
				#  printf("\n\nWord: %s\n",emptyToFull);
				#  }
				if_else_printhangman4:
					li $v1, 4
					la $a0, mistakeHangman_4
					syscall
					#jal while_loop_in_main			
				#  else if (attempts == 5){
				#  printf("\n\n  |-----|  \n  |     |\n  O     |  \n /|\\    |\n  |     |\n /      |\n        |\n -----------\n");
				#  printf("\n\nIncorrect Attempts: %d\n", attempts);
				#  printf("\nEverything incorrect you tried: %s\n", wrongattempts);
				#  printf("\n\nWord: %s\n",emptyToFull);
				#  }
				if_else_printhangman5:
					li $v1, 4
					la $a0, mistakeHangman_5
					syscall
					#jal while_loop_in_main			
				#  else if (attempts == 6){printf("\n\n  |-----|  \n  |     |\n  O     |  \n /|\\    |\n  |     |\n / \\    |\n        |\n -----------\n");
				#  printf("\n\nIncorrect Attempts: %d\n", attempts);
				#  printf("\nEverything incorrect you tried: %s\n", wrongattempts);
				#  printf("\n\nWord: %s\n",emptyToFull);
				#  lose_statement();
				#  } }
				if_else_printhangman6:
					li $v1, 4
					la $a0, mistakeHangman_6
					syscall
					jal exit
	exit:
		li $v0,10		# exit
		syscall
			
.data #All memory structures are placed after.data 
#declaring global variables and print statements
startStatement: .asciiz"\n Welcome to Hangman! \n Version 1.0 \n Implemented by Mah Noor\n\n"
exitStatement: .asciiz"\n\n Exited game \n\n" 
getUserInputStatement: .asciiz "\n\nPlease enter a guess (A-Z to play or 0 to Exit): "
correctGuessStatement: .asciiz "correct guess \n-----------------\n"
wrongGuessStatement: .asciiz "wrong guess \n-----------------\n"
numOfIncorrectStatement: .asciiz "\n\nIncorrect Attempts: "
arrayOfIncorrectAttemptsStatement: .asciiz "\nEverything incorrect you tried: "
loseStatement: .asciiz "\n\nSorry buddy, you lost.\n\n" #  printf("\n\nSorry buddy, you lost.\n\n");
winStatement: .asciiz "\n\nHip Hip, Hooray! You win! \n\n" #  printf("\n\nHip Hip, Hooray! You win! \n\n");
newLine: .asciiz "\n"
amountOfWordFilledStatement: .asciiz "\n\nWord: "
mistakeHangman_0: .asciiz "\n\n  |-----|  \n  |     |\n        |  \n        |\n        |\n        |\n        |\n -----------\n"
mistakeHangman_1: .asciiz "\n\n  |-----|  \n  |     |\n  O     |  \n        |\n        |\n        |\n        |\n -----------\n"
mistakeHangman_2: .asciiz "\n\n  |-----|  \n  |     |\n  O     |  \n  |     |\n  |     |\n        |\n        |\n -----------\n"
mistakeHangman_3: .asciiz "\n\n  |-----|  \n  |     |\n  O     |  \n /|     |\n  |     |\n        |\n        |\n -----------\n"
mistakeHangman_4: .asciiz "\n\n  |-----|  \n  |     |\n  O     |  \n /|\    |\n  |     |\n        |\n        |\n -----------\n"
mistakeHangman_5: .asciiz "\n\n  |-----|  \n  |     |\n  O     |  \n /|\    |\n  |     |\n /      |\n        |\n -----------\n"
mistakeHangman_6: .asciiz "\n\n  |-----|  \n  |     |\n  O     |  \n /|\    |\n  |     |\n / \    |\n        |\n -----------\n"

guess: .space 2#guess will be user input
attempts: .word 0 #int attempts = 0;
letterNum: .word 0 #int letterNum = 0;

answerKey1: .byte 'h','a','n','g','m','a','n' #char answerKey1[] = "h a n g m a n";
answerKey2: .byte 'H','A','N', 'G','M','A','N' #char answerKey2[] = "H A N G M A N";
emptyToFull: .byte '_', '_', '_', '_', '_', '_' ,'_' #char emptyToFull[] = "_ _ _ _ _ _ _";
wrongAttempts: .byte ' ' ,' ', ' ', ' ', ' ',' ', ' '#char wrongattempts[] = "        ";
